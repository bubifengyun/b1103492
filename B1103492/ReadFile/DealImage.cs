﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace B1103492
{
    /// <summary>
    /// 本类的目的是把所有的读取图像的操作放在一个类。
    /// 应对缺少照片等情况，造成的异常。
    /// </summary>
    static class DealImage
    {
        public static Icon GetFormIconFromFile
            (string filepath)
        {
            try
            {
                return new Icon(filepath);  
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message,
                    "丢失图片FormIcon",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                // 参考代码：
                //http://msdn.microsoft.com/en-us/library/system.drawing.icon.aspx
                return null;
            }
        }

        public static ImageBrush GetImageBrushFromUri
            (string uri)
        {
            try
            {
                // Create an ImageBrush.
                ImageBrush berriesBrush = new ImageBrush();
                berriesBrush.ImageSource =
                    new BitmapImage(
                        new Uri(uri,
                            UriKind.Relative)
                    );
                return berriesBrush;

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message,
                    "丢失图片ImageBrush",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                return null;
            }
            
        }

        public static ImageSource GetWpfIconFromFile
            (string filepath)
        {
            // 下面程序参考 help中 Window ..::.Icon Property 的例子
            try
            {
                return BitmapFrame.Create(
                    new Uri(filepath, UriKind.Relative));
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message,
                    "丢失图片WPFIcon",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                return null;
            }
        }

        public static BitmapImage GetBitmapImageFromFile
            (string filepath,int DecodePixelWidth)
        {
            try
            {
                // 参考程序：help里面的How to: Use the Image Element，有改动。
                BitmapImage myBitmapImage = new BitmapImage();
                myBitmapImage.BeginInit();
                myBitmapImage.UriSource = new Uri(filepath,UriKind.Absolute);
                myBitmapImage.DecodePixelWidth = DecodePixelWidth;// 此数越大图像越清晰
                myBitmapImage.EndInit();
                return myBitmapImage;

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message,
                    "丢失图片FishPhoto",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                return null;
            }

        }
    }
}
