﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections;
using System.Speech.Recognition;
using System.IO;

namespace B1103492
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region 启动前的准备

        public MainWindow()
        {
            InitializeComponent();
        }

        private void MainWindow_Loaded
            (object sender, RoutedEventArgs e)
        {

            this.Icon =
                DealImage.GetWpfIconFromFile("ImagesOfTitle/ico.ico");

            loadTitleGif();
            loadTitle();
            AddAllPeople();
            AddBackGroundForButtons();
        }

        private void loadTitle()
        {
            string titlePath = "Information/File/标题.txt";
            try
            {
                this.Title = File.ReadAllText(titlePath,Encoding.Default);
            }
            catch (Exception)
            {
                this.Title = "B1103492班通讯录";
            }
        }

        private void AddBackGroundForButtons()
        {
            buttonHelp.Background =
                DealImage.GetImageBrushFromUri
                    ("ImagesOfTitle/Help.png");
            buttonSearch.Background =
                DealImage.GetImageBrushFromUri
                    ("ImagesOfTitle/Search.png");
            buttonSetVoice.Background =
                DealImage.GetImageBrushFromUri
                    ("ImagesOfTitle/SetVoice.png");
            buttonAddPerson.Background =
                DealImage.GetImageBrushFromUri
                    ("ImagesOfTitle/Add.png");
            buttonDeletePerson.Background =
                DealImage.GetImageBrushFromUri
                    ("ImagesOfTitle/Delete.png");
        }

        private void loadTitleGif()
        {
            // this following code from
            // http://www.codeproject.com/Articles/23625/GIF-animation-in-WPF
            // is changed somewhere.
            System.Drawing.Color color = System.Drawing.Color.Orange;
            System.Drawing.Brush solidBrush = new System.Drawing.SolidBrush(color);
            AnimatedImageControl titleGif =
                new AnimatedImageControl
                    (this, B1103492.Properties.Resources.B1103492, solidBrush);
            titleGif.Height = TitleTopChild.Height - 5;
            
            TitleTopChild.Children.Add(titleGif);
            titleGif.HorizontalAlignment = HorizontalAlignment.Left;
            titleGif.Margin = new Thickness(5, 5, 5, 5);
        }

        public void AddAllPeople()
        {
            mainGrid.Children.Clear();
            Hashtable Persons;
            Persons = LowestData.LinqSelectString();
            mainGrid.Children.Add(new FishPhotosShowSubWindow(Persons));
        }
        #endregion

        #region 窗口各个按键的实现
        private void buttonSearch_Click(object sender, RoutedEventArgs e)
        {
            new SearchResultWindow(textBoxSearch.Text);
        }

        private void buttonHelp_Click(object sender, RoutedEventArgs e)
        {
            // 参考程序 http://www.oschina.net/code/snippet_254557_10361
            // 有改动,上面网址也是参考的别人的程序，在此不再向上追述。
            String strURL = "readme.htm";
            try
            {
                System.Diagnostics.Process.Start(strURL);
                
            }
            catch (System.ComponentModel.Win32Exception noBrowser)
            {
                if (noBrowser.ErrorCode == -2147467259)
                    MessageBox.Show(noBrowser.Message);
            }
            catch (System.Exception other)
            {
                MessageBox.Show(other.Message);
            }
        }

        private void buttonSetVoice_Click(object sender, RoutedEventArgs e)
        {
            new SetVoice().Show();
        }

        private void buttonAuthor_Click(object sender, RoutedEventArgs e)
        {
            new AboutForm().Show();
        }

        private void buttonAddPerson_Click(object sender, RoutedEventArgs e)
        {
            PersonInformationWindow piw=new PersonInformationWindow();
            piw.WindowStartupLocation=WindowStartupLocation.CenterScreen;
            piw.Show();
        }

        private void buttonDeletePerson_Click(object sender, RoutedEventArgs e)
        {
            if (
            MessageBox.Show("你要删除通讯录吗？\n想就确定，如果只是想删除某个人，"
                + "\n点击取消,然后"
                + "\n进入相应页面点击删除!",
                "删除联系人",
                MessageBoxButton.OKCancel,
                MessageBoxImage.Warning,
                MessageBoxResult.Cancel)
                == MessageBoxResult.OK)
            {
                LowestData.psuDeleteAll();
                AddAllPeople();
            }
        }

        private void buttonViewAllPerson_Click(object sender, RoutedEventArgs e)
        {
            String pathOfAllPerson2003 = 
                Environment.CurrentDirectory
                +"/"
                +"Information/File/文本通讯录.xls";
            // 下面是备用地址，如果上面打不开，就用下面这种形式
            String pathOfAllPerson2007 =
                Environment.CurrentDirectory
                + "/"
                + "Information/File/文本通讯录.xlsx";
                
            try
            {
                System.Diagnostics.Process.Start(pathOfAllPerson2003);
            }
            catch (System.IO.FileNotFoundException)
            {
                System.Diagnostics.Process.Start(pathOfAllPerson2007);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message
                    +"\n您没有\n“" + pathOfAllPerson2003 
                    + "”\n或者\n“" + pathOfAllPerson2007+"”\n"
                    +"建议您把要打开的通信录放到上述目录下\n"
                    +"或者没有安装打开.xls,.xlsx的软件，或文件损坏",
                    "文件打开失败",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }
        #endregion

        #region 语音识别的实现(SpeechToText)

        private bool isInitializeSpeechToText = false;
        private void GetTextFromSpeech
            (object sender, EventArgs e)
        {
            textBoxSearch.Text = sender as string;
            new SearchResultWindow(textBoxSearch.Text);
        }

        /// <summary>
        /// 当鼠标进入到textBoxSearch，开始语音识别
        /// </summary>
        private void textBoxSearchForSpeechToText_GotFocus
            (object sender, RoutedEventArgs e)
        {
            // EventSpeechRecognized是上面的函数，
            // 实现识别后的处理。
            // LowestData.GetNames()是获取可以识别的词库
            // 本语音识别，只可以识别姓名。
            if (!isInitializeSpeechToText)
            {
                SpeechToText.Initialize(
                    LowestData.GetNames(),
                    RecognizeMode.Multiple);

                SpeechToText.GetTextFromSpeech += new EventHandler(GetTextFromSpeech);
                isInitializeSpeechToText = true;

            }
            SpeechToText.Start();
        }

        private void textBoxSearchForSpeechToText_LostFocus(object sender, RoutedEventArgs e)
        {
            SpeechToText.End();
        }
        #endregion

        #region 关闭程序保存数据
        private void Window_Closed(object sender, EventArgs e)
        {
            // 关闭程序前，保存数据。
            LowestData.saveToXml();

            App.MyNotifyIcon.Visible = false;

            // 下面这句话必须放到最后，否则他后面的不能执行。
            Environment.Exit(0);
        }
        #endregion
    }
}
