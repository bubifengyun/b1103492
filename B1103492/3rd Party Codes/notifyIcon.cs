﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Windows;

namespace B1103492
{
    /// <summary>
    /// 本类致力于只要稍作修改，就可以方便使用的类。
    /// 减少把notifyIcon放到其他类的可能性。
    /// 当然可移植性不是很高，但是也足够了。
    /// </summary>
    public class notifyIcon
    {
        #region 公用部分按钮的设置
        private NotifyIcon _notifyIcon = new NotifyIcon();

        public notifyIcon()
        {
            _notifyIcon.Icon = 
                DealImage.GetFormIconFromFile
                    ("ImageSOfTitle/ico.ico");
            _notifyIcon.Text = "通讯录";
            _notifyIcon.Visible = true;
            addContextMenuStrip();
        }

        public bool Visible
        {
            get
            {
                return _notifyIcon.Visible;
            }
            set
            {
                _notifyIcon.Visible = value;
            }
        }

        /// <summary>
        /// 下面代码的来源，是通过在Window.Form图形化界面建立ContextMenuStrip，
        /// 然后获取.Design.cs的代码，作了修改放进这里的。
        /// 这个代码可以单独使用的。只需要做部分修改即可。
        /// 把它放进了3rd Party Codes
        /// </summary>
        private void addContextMenuStrip()
        {
            ToolStripMenuItem aboutToolStripMenuItem = 
                new ToolStripMenuItem("关于");
            aboutToolStripMenuItem.Click +=
                new EventHandler(aboutToolStripMenuItem_Click);

            ToolStripMenuItem quitToolStripMenuItem = 
                new ToolStripMenuItem("退出");
            quitToolStripMenuItem.Click +=
                new EventHandler(quitToolStripMenuItem_Click);

            ContextMenuStrip cms = new ContextMenuStrip();
            cms.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            aboutToolStripMenuItem,
            quitToolStripMenuItem});

            _notifyIcon.ContextMenuStrip = cms;
            
        }

        void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LowestData.saveToXml();
            this.Visible = false;
            Environment.Exit(0);
        }

        void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new AboutForm().Show();
        }
        #endregion

        #region 专为Window设置的部分
        Window _w;

        public notifyIcon(Window w)
            : this()
        {
            _w = w;
            _notifyIcon.Click+=new EventHandler
                (_notifyIconForWindow_Click);
        }

        void _notifyIconForWindow_Click
            (object sender, EventArgs e)
        {
            WindowStateChange();
        }

        private void WindowStateChange()
        {
            if (_w.WindowState != WindowState.Minimized)
            {
                _w.WindowState = WindowState.Minimized;
            }
            else
            {
                _w.WindowState = WindowState.Maximized;
            }
        }
        #endregion

        #region 专为Form设置的部分
        Form _f;

        public notifyIcon(Form f)
            : this()
        {
            _f = f;
            _notifyIcon.Click+=new EventHandler
                (_notifyIconForForm_Click);
        }

        void _notifyIconForForm_Click
            (object sender, EventArgs e)
        {
            FormStateChange();
        }

        private void FormStateChange()
        {
            if (_f.WindowState!=FormWindowState.Minimized)
            {
                _f.WindowState = FormWindowState.Minimized;
            }
            else
            {
                _f.WindowState = FormWindowState.Maximized;
            }
        }
        #endregion
    }
}
