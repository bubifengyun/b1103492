﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Drawing;

namespace B1103492
{
    /// <summary>
    /// Interaction logic for SetVoice.xaml
    /// </summary>
    public partial class SetVoice : Window
    {
        public SetVoice()
        {
            InitializeComponent();
            this.Background =
                DealImage.GetImageBrushFromUri
                    ("ImagesOfTitle/voiceSettingBackground.jpg");

            this.Icon = 
                DealImage.GetWpfIconFromFile("ImagesOfTitle/ico.ico");

            initializeSet();
            this.ShowInTaskbar = false;
        }

        private void initializeSet()
        {
            Gender.Text = ReadWriteSpeechAndTextSettingFile.readGenderFromSettingFile();
            Volume.Value = ReadWriteSpeechAndTextSettingFile.readVolumeFromSettingFile();
            Rate.Value = ReadWriteSpeechAndTextSettingFile.readRateFromSettingFile();
        }

        private void CancelModify_Click(object sender, RoutedEventArgs e)
        {
            this.Close(); 
        }

        private void ConfirmModify_Click(object sender, RoutedEventArgs e)
        {
            // 调试中，发现该语句放到最后面，1-2秒内关闭窗口，会比较卡
            // 放到开始，发现好多了。
            this.Close();

            // 保存数据
            ReadWriteSpeechAndTextSettingFile.writeGender_Volume_RateToSettingFile
                (Gender.Text, Volume.Value, Rate.Value);

            // 实时更新发音设置，避免设置后下次才有效
            TextToSpeech.readSetVoiceFromSettingFile();
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            (sender as Slider).ToolTip = (int)(sender as Slider).Value;
        }        
    }
}
