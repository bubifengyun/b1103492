﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Speech.Synthesis;
using System.Windows;

namespace B1103492
{
    class TextToSpeech
    {
        /// <summary>
        /// 代码来源：http://www.codeproject.com/KB/audio-video/TextToSpeech.aspx 
        /// 做了部分改动
        /// </summary>
        static private SpeechSynthesizer voice = new SpeechSynthesizer();

        static public void readSetVoiceFromSettingFile()
        {
            try
            {
                switch (ReadWriteSpeechAndTextSettingFile.readGenderFromSettingFile())
                {
                    case "默认":
                        voice.SelectVoiceByHints(VoiceGender.NotSet); break;
                    case "男声":
                        voice.SelectVoiceByHints(VoiceGender.Male); break;
                    case "女声":
                        voice.SelectVoiceByHints(VoiceGender.Female); break;
                    case "中性":
                        voice.SelectVoiceByHints(VoiceGender.Neutral); break;
                }
                
                voice.Volume = (int)ReadWriteSpeechAndTextSettingFile.readVolumeFromSettingFile();
                voice.Rate = (int)ReadWriteSpeechAndTextSettingFile.readRateFromSettingFile();

            }
            catch (Exception)
            {
                MessageBox.Show("Speak error", "Text to Speak");
            }
        }

        static public void readString(string text)
        {
            readSetVoiceFromSettingFile();
            voice.SpeakAsync(text);
        }

        static public void readKeyNonEnglishString(string keyText)
        {

            voice.SpeakAsyncCancelAll();
            voice.Volume = 100;
            voice.Rate = -10;
            foreach (char c in keyText)
            {
                voice.SpeakAsync(c.ToString());
            }
        }

        static public void stopRead()
        {
            voice.SpeakAsyncCancelAll();
        }
    }
}
