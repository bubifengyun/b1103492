﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows;

namespace B1103492
{
    public class ReadWriteSpeechAndTextSettingFile
    {
        private static string GetSettingFilesPath()
        {
            return  "Information/File/声音设置.ini";
        }
        public static void writeGender_Volume_RateToSettingFile
            (string GenderText, double VolumeValue, double RateValue)
        {
            string[] Sets ={
                                  "发声性别 = " + GenderText,
                                  "音    量 = " + VolumeValue,
                                  "语    速 = " + RateValue,
                                  " "," ",
                                  "------------------------------------------ ",
                                  "说明:",
                                  "发声性别只可以选择:默认，男声，女声，中性",
                                  "音量只可以选择0-100的实数",
                                  "语速只可以选择-10到10之间的实数"
                            };
            File.WriteAllLines(GetSettingFilesPath(), Sets);
        }

        public static string readGenderFromSettingFile()
        {
            try
            {
                string[] lines = File.ReadAllLines(GetSettingFilesPath());
                foreach (string line in lines)
                {
                    var parts = line.Split(new char[] { '=' });
                    if (parts[0].Trim() == "发声性别")
                        return parts[1].Trim();
                }
            }
            catch (Exception) { }
            return "默认";
        }

        public static double readVolumeFromSettingFile()
        {
            try
            {
                string[] lines = File.ReadAllLines(GetSettingFilesPath());
                foreach (string line in lines)
                {
                    var parts = line.Split(new char[] { '=' });
                    if (parts[0].Trim() == "音    量")
                        return Double.Parse(parts[1]);
                }
            }
            catch(Exception){}
            return 100;
        }

        public static double readRateFromSettingFile()
        {
            try
            {
                string[] lines = File.ReadAllLines(GetSettingFilesPath());
                foreach (string line in lines)
                {
                    var parts = line.Split(new char[] { '=' });
                    if (parts[0].Trim() == "语    速")
                        return Double.Parse(parts[1]);
                }
            }
            catch (Exception) { }
            return 0;
        }
    }
}
