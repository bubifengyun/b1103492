﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using System.Threading;

namespace B1103492
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>

        // 参考文献：
        // http://www.codeproject.com/Articles/16901/Falling-Snow-on-Your-Desktop-The-C-Version
        // 有改动
        // 确保本电脑上只有一个本程序运行。

        private static Mutex mutex;
        public static MainWindow MyMainWindow;
        public static notifyIcon MyNotifyIcon;
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            mutex = new Mutex(true, "通讯录");

            if (mutex.WaitOne(0, false))
            {
                // 增大开始前显示splashWindow的时间
                // 就放在了开头，未必真能如此。
                LowestData.readFromXml();

                MyMainWindow = new B1103492.MainWindow();
                MyMainWindow.Show();
                MyNotifyIcon = new notifyIcon(MyMainWindow);
                MyNotifyIcon.Visible = true;
            }
            else
            {
                MessageBox.Show("您已经打开了一个通讯录，\n不可以同时打开两个的。",
                    "出错了！",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error,
                    MessageBoxResult.OK);
                Environment.Exit(0);
            }
        }
    }
}
