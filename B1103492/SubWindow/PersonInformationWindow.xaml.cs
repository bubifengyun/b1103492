﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace B1103492
{
    /// <summary>
    /// 本类的目的是显示更新添加删除个人信息
    /// </summary>
    public partial class PersonInformationWindow : Window
    {
        // 定义该变量的目的是实现图片完全用 
        // string photoRelativeName代替
        // 方便使用。
        public string photoRelativeName
        {
            get
            {
                try
                {
                    return (buttonImageScr.Background as ImageBrush)
                        .ImageSource.ToString();
                }
                catch (Exception)
                {
                    return null;
                }

            }
            set
            {
                buttonImageScr.Background =
                    DealImage.GetImageBrushFromUri(value);
            }

        }

        #region 初始化窗口及读取数据
        public PersonInformationWindow()
        {
            InitializeComponent();
            gridHead.Background =
                DealImage.GetImageBrushFromUri
                    ("ImagesOfTitle/male0.png");
            gridTailor.Background =
                DealImage.GetImageBrushFromUri
                    ("ImagesOfTitle/male1.png");
            photoRelativeName =
                "ImagesOfTitle/default.jpg";

            this.ShowInTaskbar = false;
        }

        public PersonInformationWindow(Informations infor):this()
        {
            try
            {
                this.name.Text = infor.name;
                this.StuNo.Text = infor.studentNumber;
                this.QQ.Text = infor.qq;
                this.Phone.Text = infor.phone;
                this.Email.Text = infor.email;
                this.Room.Text = infor.room;
                this.Lab.Text = infor.lab;
                this.Master.Text = infor.master;
                this.photoRelativeName = infor.photoRelativeName;
                this.home.Text = infor.home;
                this.ps.Text = infor.ps;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        #endregion

        #region 按钮功能的实现
        private void buttonDeleteOrquit_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult r = MessageBox.Show("您确认放弃添加或者删除此人吗？",
                "提示信息",
                MessageBoxButton.YesNo,
                MessageBoxImage.Question, MessageBoxResult.No);

            if (r == MessageBoxResult.Yes)
            {
                LowestData.psuDelete(this.name.Text);
                App.MyMainWindow.AddAllPeople();
                this.Close();
            }
            
        }

        private void buttonClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void buttonAddOrUpdate_Click(object sender, RoutedEventArgs e)
        {
            //该语句表示如果保存成功则关闭对话框。
            if (LowestData.psuAddUpdate(new Informations
                (this.name.Text, this.StuNo.Text, this.QQ.Text,
                this.Phone.Text, this.Email.Text, this.Room.Text,
                this.Lab.Text, this.Master.Text, this.photoRelativeName,
                this.home.Text, this.ps.Text)))
            {
                App.MyMainWindow.AddAllPeople();
                this.Close();
            }
        }

        private void buttonImageScr_Click(object sender, RoutedEventArgs e)
        {
            AddPersonPhotoWindow apw = new AddPersonPhotoWindow(this);
            apw.Show();
            apw.Owner = this;
            this.IsEnabled = false;
        }
        #endregion

        #region 实现窗口的拖动
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                this.DragMove();
            }
            catch (Exception)
            {
            }
        }
        #endregion
    }
}
