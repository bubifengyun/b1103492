﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace B1103492
{
    public partial class AboutForm : Form
    {
        public AboutForm()
        {
            InitializeComponent();
            AddPhoto();

            // 可采用如下的方式添加图标
            this.Icon = DealImage.GetFormIconFromFile("ImagesOfTitle/ico.ico");
            // 也可以采用这样的方式，
            // 前提是先把相应文件加入到Properities.Resources中去
            // this.Icon = global::B1103492.Properties.Resources.ico;
            this.ShowInTaskbar = false;
        }

        private void AddPhoto()
        {
            try
            {
                /// <summary>
                /// 参考程序：help 里面的 PictureBox Class提供的例子
                /// </summary>
                Bitmap bm = new Bitmap("ImagesOfTitle/SJTUcicle.jpg");

                pictureBoxSJTU.SizeMode =
                    PictureBoxSizeMode.StretchImage;
                pictureBoxSJTU.Image = bm as Image;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void OnHome(object sender, LinkLabelLinkClickedEventArgs e)
        {
            String strURL = "http://www.oschina.net/code/snippet_254557_11202";
            try
            {
                System.Diagnostics.Process.Start(strURL);
            }
            catch (System.ComponentModel.Win32Exception noBrowser)
            {
                if (noBrowser.ErrorCode == -2147467259) MessageBox.Show(noBrowser.Message);
            }
            catch (System.Exception other)
            {
                MessageBox.Show(other.Message);
            }
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
