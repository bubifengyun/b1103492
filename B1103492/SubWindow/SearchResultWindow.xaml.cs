﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections;

namespace B1103492
{
    /// <summary>
    /// Interaction logic for SearchResutlWindow.xaml
    /// </summary>
    public partial class SearchResultWindow : Window
    {
        #region 窗口初始化
        public SearchResultWindow()
        {
            InitializeComponent();
            backgroundAndIco();
            this.Show();
        }

        private void backgroundAndIco()
        {
            this.Background =
                DealImage.GetImageBrushFromUri
                    ("ImagesOfTitle/voiceSettingBackground.jpg");

            this.Icon =
                DealImage.GetWpfIconFromFile("ImagesOfTitle/ico.ico");

            this.ShowInTaskbar = false;
        }
        #endregion

        #region 查询部分的实现
        public SearchResultWindow(string searchText):this()
        {
            doSearch(searchText);
           
        }

        private void doSearch(string searchText)
        {
            Hashtable searchResult =
                LowestData.LinqSelectString(searchText);

            switch (searchResult.Count)
            {
                case 0:
                    this.Close();
                    MessageBox.Show("没有满足\n“"
                        + searchText + "”\n"
                        + "的人员",
                        "查询结果",
                        MessageBoxButton.OK,
                        MessageBoxImage.Information);
                    break;
                case 1:
                    this.Close();
                    foreach (Informations ifm
                        in searchResult.Values)
                    {
                        new PersonInformationWindow(ifm)
                            .Show();
                    }
                    break;
                default:
                    result.Children.Add
                        (new FishPhotosShowSubWindow
                            (searchResult));
                    break;
            }
        }
        #endregion
    }
}
