﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace B1103492
{
    /// <summary>
    /// Interaction logic for AddPersonPhotoWindow.xaml
    /// </summary>
    public partial class AddPersonPhotoWindow : Window
    {
        #region 初始化窗口
        // 本例借鉴了Design Patterns in C#里面的
        // Observer Pattern里面传递窗口对象的方法。
        // 下面personInformationWindow就是用来存储窗口对象的。
        // 相当于直接对窗口对象操作。
        private PersonInformationWindow personInformationWindow;

        public AddPersonPhotoWindow()
        {
            InitializeComponent();
            backgroundAndIco();

            FishPhotosShowSubWindow psw = new FishPhotosShowSubWindow(this);

            
            choosePhoto.Children.Add(psw);
        }

        public AddPersonPhotoWindow(PersonInformationWindow p):this()
        {
            personInformationWindow = p;
        }

        private void backgroundAndIco()
        {
            this.Background =
                DealImage.GetImageBrushFromUri
                    ("ImagesOfTitle/voiceSettingBackground.jpg");


            this.Icon = 
                DealImage.GetWpfIconFromFile("ImagesOfTitle/ico.ico");

            this.ShowInTaskbar = false;
        }
        #endregion

        // 打开AddPersonPhotoWindow，
        // 锁定原来的窗口PersonInformationWindow,
        // 关闭该窗口时，则要解锁。
        private void Window_Closed(object sender, EventArgs e)
        {
            personInformationWindow.IsEnabled = true;
        }

        // 传递选择到的照片
        public void chooseImageAndClose_MouseDown
            (object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            personInformationWindow.photoRelativeName = "Information/Photo/"+(sender as Image).ToolTip.ToString();
            this.Close();
            
        }
    }
}
